<?php

/**
 * @file
 * Template to customize the text of the non-value first option.
 * v6.x-2.1+
 *
 * Available variables:
 *
 * - $required:   Either TRUE (if the field is required) or FALSE (if the field
 *                is not required).
 *
 * - $option:     The text to be printed within the <option>...</option>
 *                HTML element.
 *                Default: If $required (see above) is TRUE, the translatable
 *                string "- Please select an option -"; otherwise the
 *                translatable string "- No selection -".
 *
 * - $field_name: The name of the CCK field.
 *                eg. "field_profile_gender"
 *
 * - $type_name:  The name of the content type.
 *                eg. "profile"
 *
 * - $path:       The path of the currently loaded page.
 *                eg. "admin/user/user/create", "user/123/edit/profile",
 *                "user/register", "myalias/register"
 *
 * - $site:       The site's domain name, without leading "www.".
 *                For example, the site called by http://www.example.com/
 *                has $site set to "example.com".
 *                Useful if you share a theme to more than one site in a
 *                multisite environment, having it installed under
 *                "sites/all/themes".
 *
 */
?>
<?php

//if ($type_name == 'profile') {
//  // The form to add or edit a user, when the "Content Profile" module is installed
//  $option = $required ? t('- Please select an option for this user -') : t('- No selection for this user -');
//}

print $option;
