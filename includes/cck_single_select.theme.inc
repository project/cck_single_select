<?php

/**
 * @file
 * Part of the "CCK Single Select" module that will be included for theming.
 */

/**
 * Helper function to return an array to cck_required_single_select_theme().
 */
function _cck_single_select_theme_array($existing, $type, $theme, $path) {
  $module_name = 'cck_single_select';
  $hook_name = $module_name;
  $template_base_filename = preg_replace('/_/', '-', $hook_name);
  return array(
    $hook_name => array(
      'file' => 'includes/' . $module_name . '.theme.inc',
      'template' => 'templates/' . $template_base_filename,
      // Template file name = "cck-single-select.tpl.php"
      'arguments' => array(
        'option' => NULL,
      ),
    ),
  );
}

/**
 * template_preprocess_HOOK
 * https://api.drupal.org/api/drupal/includes!theme.inc/6
 *
 * Provide variables to be used in "cck-single-select.tpl.php".
 */
function template_preprocess_cck_single_select(&$variables) {
  $variables['required'] = $variables['option']['required'];
  $variables['field_name'] = $variables['option']['field_name'];
  $variables['type_name'] = $variables['option']['type_name'];
  $variables['option'] = $variables['required'] ? t('- Please select an option -') : t('- No selection -');
  global $base_url;
  $site = preg_replace("/^[^\/]+[\/]+/", '', $base_url);
  $site = preg_replace("/[\/].+/", '', $site);
  $site = preg_replace("/^www[^.]*[.]/", '', $site);
  $variables['site'] = $site;
  $variables['path'] = implode('/', arg(NULL, drupal_get_path_alias($_GET['q'])));
}
